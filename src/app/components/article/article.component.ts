import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiServicesService, Article } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input()
  public article?:Article;
  @Output()
  modify = new EventEmitter();
  @Output()
  deleted = new EventEmitter();
  @Input()
  public homePage=false;
  public modifyContent:boolean=false;

  modifContent = new FormGroup({
    content: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(26),
    ]),
  });



  constructor(public apiService:ApiServicesService) {
    this.apiService.deleteArticle;
   }
  
  modifyButton(){
    if(this.modifyContent){
      this.modifyContent=false;
    }else{
      this.modifyContent=true;
    }
  }
  async submitContent(){
    const id=this.article?.id;
    if(id)await this.apiService.modifyArticle(this.modifContent.value,id);
    this.apiService.loadArticles();
    if (id)this.article=await this.apiService.getArticle(id);
    this.modifContent.reset();
    this.modifyContent=false;
  }
  deleteButton(){
    this.deleted.emit(this.article);
  }
  updateForm(){
    this.modifContent.controls['content'].setValue(this.article?.content)
  }
  ngOnInit(){
    this.updateForm();
  }

}
