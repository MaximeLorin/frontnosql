import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { LoginServiceService } from 'src/app/services/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 
  logUser = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(26),
    ]),
    // email: new FormControl('', [
    //   Validators.required,
    //   Validators.minLength(8),
    //   Validators.maxLength(26),
    // ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(26),
    ]),
  });

  submitLogUser() {
   this.loginService.login(this.logUser.value.username,this.logUser.value.password)
    this.router.navigate(['/']);
  }

  deconnect(){
   this.loginService.logout();
  }
  constructor(private router: Router, public loginService:LoginServiceService) { 
 
  }

  ngOnInit(): void {
  }

}
