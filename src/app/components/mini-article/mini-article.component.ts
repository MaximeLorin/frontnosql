import { Component, Input, OnInit } from '@angular/core';
import { ApiServicesService, Article } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-mini-article',
  templateUrl: './mini-article.component.html',
  styleUrls: ['./mini-article.component.scss']
})
export class MiniArticleComponent implements OnInit {
  @Input()
  public article?:Article;
  constructor(private apiService:ApiServicesService) { }
  
  clearSearchList(){
    console.log( "tototo");
    this.apiService.articleSearchList=[];
    console.log( this.apiService.articleSearchList);
    
  }
  
  ngOnInit(): void {
  }

}
