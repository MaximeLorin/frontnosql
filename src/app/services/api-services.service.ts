import { Injectable } from '@angular/core';
import axios from 'axios';

export interface Article{
  id:string,
  title:string,
  image:string,
  summary:string,
  content:string,
  draft:boolean,
}

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {
  public articleList:Article[]=[];
  public articleSearchList:Article[]=[]
  private urlBase="http://localhost:8080/api/articles"

  constructor() { }

  public async getAllArticles():Promise<Article[]>{
      const list=await axios.get(this.urlBase);
      let data=list.data;
      return data.map((article:any)=>{
        return{
          id:article.id,
          title:article.title,
          image:article.image,
          summary:article.summary,
          content:article.content,
          draft:article.draft
        }
      })   
  }

  public async getArticle(id:string):Promise<Article>{
    const article = axios.get<Article>(this.urlBase+"/"+id);
    let data=(await article).data;
    return data;
  }

  public async addArticle(article:Article){
    const newArticle= await axios.post(this.urlBase,article);
    this.articleList.push(newArticle.data);
  }

  public async modifyArticle(article:Article, id:string){
    return await axios.patch(this.urlBase+"/"+id,article);;
  }

  public async deleteArticle(article:Article){
    const index: number = this.articleList.indexOf(article);
    this.articleList = this.articleList.filter((u) => u !== article);
    return await axios.delete(this.urlBase+"/"+article.id);
  }
  public async getSearchArticles(word:string):Promise<Article[]>{
    const list=await axios.get(this.urlBase+"/searchPart",{ params: { title: word }});
    let data= list.data;
      return data.map((article:any)=>{
        return{
          id:article.id,
          title:article.title,
          image:article.image,
          summary:article.summary,
          content:article.content,
          draft:article.draft
        }
      })   
  }
  public async loadArticlesSearch(word:string){
    this.articleSearchList=await this.getSearchArticles(word);
  }
  public async loadArticles(){
    this.articleList=await this.getAllArticles();
    console.log(this.articleList);
    
  }
  
}
