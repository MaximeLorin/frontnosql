import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddArticlePageComponent } from './pages/add-article-page/add-article-page.component';
import { ArticlePageComponent } from './pages/article-page/article-page.component';
import { HomeComponent } from './pages/home/home.component';
import { PageLogSubComponent } from './pages/page-log-sub/page-log-sub.component';

const routes: Routes = [{
  path:'', component:HomeComponent
},{
  path:'article/:id', component:ArticlePageComponent,
  
},
{path:'newarticle', component:AddArticlePageComponent},
{path:'connect/:type', component:PageLogSubComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
