import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleComponent } from './components/article/article.component';
import { HomeComponent } from './pages/home/home.component';
import { ArticlePageComponent } from './pages/article-page/article-page.component';
import { AddArticlePageComponent } from './pages/add-article-page/add-article-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PageLogSubComponent } from './pages/page-log-sub/page-log-sub.component';
import { LoginComponent } from './components/login/login.component';
import { SubscribeComponent } from './components/subscribe/subscribe.component';
import { MiniArticleComponent } from './components/mini-article/mini-article.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    HomeComponent,
    ArticlePageComponent,
    AddArticlePageComponent,
    PageLogSubComponent,
    LoginComponent,
    SubscribeComponent,
    MiniArticleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
