import { Component, Input, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { ApiServicesService } from './services/api-services.service';
import { LoginServiceService } from './services/login-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'NoSqlBlogAngular';
  type:string[]=["login","subscribe"];
  
  searchContent: string="";

  searchForm = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(26),
    ])
  })
  constructor(public apiServices: ApiServicesService,public loginService: LoginServiceService,private router:Router) {
 
    this.loginService.interceptorError(this.router);
    this.loginService.intercetor();
  }
  async ngOnInit()  {
    this.searchForm.valueChanges.subscribe(changes => {
      this.searchContent=changes.title;
      this.apiServices.loadArticlesSearch(this.searchContent);
      console.log(this.apiServices.articleSearchList)
      
  });
  }
  deconnect(){
    this.loginService.logout();
  }
}
