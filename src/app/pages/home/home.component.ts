import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  searchContent: string="";

  searchForm = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(26),
    ])
  })
  constructor(public apiServices:ApiServicesService) {
    this.apiServices.deleteArticle;
  }
  submitSearch(){
    this.searchForm.value.array.forEach((element: any)=> {
      console.log(element);
      
    });
  }
 
  async ngOnInit()  {
    await this.apiServices.loadArticles();
   
    this.searchForm.valueChanges.subscribe(changes => {
      this.searchContent=changes.title;
      this.apiServices.loadArticlesSearch(this.searchContent);
      console.log(this.apiServices.articleSearchList)
      
  });
  }

}
