import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageLogSubComponent } from './page-log-sub.component';

describe('PageLogSubComponent', () => {
  let component: PageLogSubComponent;
  let fixture: ComponentFixture<PageLogSubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageLogSubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageLogSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
