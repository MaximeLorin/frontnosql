import { Component, OnChanges, OnInit, SimpleChange } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-log-sub',
  templateUrl: './page-log-sub.component.html',
  styleUrls: ['./page-log-sub.component.scss']
})
export class PageLogSubComponent implements OnInit {
  pageType:string;
  
  constructor(private activatedRoute: ActivatedRoute) {
    this.pageType=this.activatedRoute.snapshot.params['type'] 
    console.log(this.pageType);
  }
  
  ngOnInit(){
    this.activatedRoute.paramMap.subscribe(() => {
      console.log(this.pageType);
      this.pageType=this.activatedRoute.snapshot.params['type'] 
  });

  }

}
