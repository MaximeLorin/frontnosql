import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-add-article-page',
  templateUrl: './add-article-page.component.html',
  styleUrls: ['./add-article-page.component.scss'],
})
export class AddArticlePageComponent implements OnInit {
  public publish:boolean=false;
  public open:boolean=false;
  public articleString:string="+ Nouvelle Article"

  createArticle = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(26),
    ]),
    image: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(500),
    ]),
    summary: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(50),
    ]),
    content: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(800),
    ]),
    draft: new FormControl('', [ Validators.required,]),
  });

  submitArticle() {
    this.apiService.addArticle(this.createArticle.value);
    console.log(this.createArticle.value);
    
    this.apiService.loadArticles();
    
  }

  onPublishBool(value:boolean){
    this.publish=value;
    console.log(this.publish);
    
  }
  constructor(private apiService: ApiServicesService, private router: Router) {
    this.apiService.addArticle;
  }

  openSubscribe(){
    this.open=!this.open;
    if(this.open){
      this.articleString="- Fermer"
    }else{
      this.articleString="+ Nouvelle Article"
    }
  }
  ngOnInit(): void {}
}
