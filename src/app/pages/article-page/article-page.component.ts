import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServicesService, Article } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-article-page',
  templateUrl: './article-page.component.html',
  styleUrls: ['./article-page.component.scss']
})
export class ArticlePageComponent implements OnInit {
  public article?:Article;
  public articleId?:string;
  
  constructor(public apiService:ApiServicesService,private activatedRoute: ActivatedRoute,private router: Router) { 
    this.apiService.deleteArticle;
    this.articleId = this.activatedRoute.snapshot.params['id'];
    const article= this.apiService.articleList.find((article)=>article.id===this.articleId);

    if(!article){
      return;
    }
    this.article=article;

  }

  returnHome(){
    this.router.navigate(['/']);
  }
  async ngOnInit() {
    if(!this.article && this.articleId){
      try {
        this.article=await this.apiService.getArticle(this.articleId);
      } catch (error) {
        this.router.navigate(['/']);
      }
      
    }
  }

}
